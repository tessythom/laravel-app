<?php

namespace Tests\Feature;

use App\Product;
use http\Env\Request;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateProduct()
    {
        $data = [
            'title' => "New Product",
            'abstract' => "New Product",
            'description' => "This is a product",
            'stock' => 20,
            'price' => 288,
            'image_url' => "https://lorempixel.com/640/480/?82071"
        ];
        $response = $this->json('POST', '/api/products',$data);
        $response->assertStatus(200);
        $response->assertJson(['status' => true]);
        $response->assertJson(['message' => "Product Created!"]);

    }

    public function testGettingAllProducts()
    {
        $response = $this->json('GET', '/api/products');
        $response->assertStatus(200);
       // $response->assertJson(['data' => $response]);
    }

    /*public function testGettingOneProduct($title)
    {
        $response = $this->json('GET', '/api/product/{$title}');
        $response->assertStatus(200);
        // $response->assertJson(['data' => $response]);
    }*/
}
