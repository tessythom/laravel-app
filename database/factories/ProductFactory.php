<?php

use Faker\Generator as Faker;


$factory->define(App\Product::class, function (Faker $faker) {
    return [
        //
        'title' => $faker->sentence(7,20),
        'abstract' => $faker->sentence(7,15),
        'description' => $faker->paragraphs(rand(10,15),true),
        'price' => $faker->numberBetween(300,1000),
        'image_url' => $faker->imageUrl($width = 640, $height = 480),
        'stock' =>    $faker->numberBetween(3,15),
        'created_at' => $faker->dateTime('now',null) ,
        'updated_at' => $faker->dateTime('now',null)
    ];
});
