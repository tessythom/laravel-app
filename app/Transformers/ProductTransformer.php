<?php

namespace App\Transformers;

use App\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    public function transform(Product $prod)
    {
        return [
            'title' => $prod->title,
            'price' => $prod->price,
            'stock' => $prod->stock,
            'self' =>  'projdev.local/product/'.$prod->id
        ];
    }

}