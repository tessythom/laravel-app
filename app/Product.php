<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Product extends Model
{
    //
    use Uuids;
    //public $timestamps = false;
    public $incrementing = false;
    public $fillable = ['title','price','abstract','stock','image_url','created_at','updated_at'];
}
