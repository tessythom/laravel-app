<?php

namespace App\Http\Controllers;

use App\Product;
use App\Transformers\ProductTransformer;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;


class ProductsController extends Controller
{
    //
    private $fractal;
    private $productTransformer;

    function __construct(Manager $fractal, ProductTransformer $productTransformer)
    {
        $this->fractal = $fractal;
        $this->productTransformer = $productTransformer;
    }

    public function index(Request $request)
    {
            $prodsCount = Product::count();

            if($request->max){$max = $request->max;} else {$max = 10;}
            if($prodsCount > 0) {
                //$prodsPaginator = Product::paginate($max)->appends(request()->query());// Get products from DB
                $prodsPaginator = Product::paginate($max)->appends(request()->query());
                $prods = new Collection($prodsPaginator->items(), $this->productTransformer); // Create a resource collection transformer
                $prods->setPaginator(new IlluminatePaginatorAdapter($prodsPaginator));
                $prods = $this->fractal->createData($prods); // Transform data
                return $prods->toArray(); // Get transformed array of data
            }
            else {return $this->respondValidationError('No Records Available','error');}
    }

    public function getProduct( $id ){
        $prod = Product::where('id', '=', $id)->first();
        if($prod){return response()->json( $prod,200);}
        else{return $this->respondValidationError('Invalid Product','error');
            /*return response()->json([
                'status' => 'error',
                'message' => 'Invalid Product'
            ]);*/
        }
    }

    public function postNewProduct(Request $request){

        $rules = array (
            'title' => 'required|max:50',
            'abstract' => 'required|max:70',
            'price' => 'required|integer',
            'stock' => 'required|integer'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator-> fails()){
            return $this->respondValidationError('Fields Validation Failed.','error', $validator->errors());
            /*return response()->json([
                'status' => 'error',
                'message' => 'Fields Validation Failed.'
            ]);*/
        }
        else {
            $product = Product::create([
                'title' => $request->title,
                'abstract' => $request->abstract,
                'description' => $request->description,
                'stock' => $request->stock,
                'price' => $request->price,
                'image_url' => $request->image_url
            ]);
            /*return response()->json([
                'status' => (bool) $product,
                'message' =>  'Product Created!'
            ]);*/
            return $this->respondValidationError('Product Created!',true ,$product);
        }

    }

    public function respondValidationError($message, $status = null,$data=null){
        return ([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }



}
